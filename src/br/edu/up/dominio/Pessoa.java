package br.edu.up.dominio;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pessoa {

  private Integer id;
  private String nome;
  private String rg;
  private String cpf;
  private String estadoCivil;
  private char sexo;
  private Date dataNascimento;
  private String local;
  
  public Pessoa() {
  }

  public Pessoa(Integer id, String nome, String rg, String estadoCivil, char sexo,
      Date dataNascimento, String local) {
    super();
    this.id = id;
    this.nome = nome;
    this.rg = rg;
    this.estadoCivil = estadoCivil;
    this.sexo = sexo;
    this.dataNascimento = dataNascimento;
    this.local = local;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getRg() {
    return rg;
  }

  public void setRg(String rg) {
    this.rg = rg;
  }

  public String getEstadoCivil() {
    return estadoCivil;
  }

  public void setEstadoCivil(String estadoCivil) {
    this.estadoCivil = estadoCivil;
  }

  public char getSexo() {
    return sexo;
  }

  public void setSexo(char sexo) {
    this.sexo = sexo;
  }

  public Date getDataNascimento() {
    return dataNascimento;
  }

  
//  public void setData(String strData) throws ParseException {
//    SimpleDateFormat sf = new SimpleDateFormat("ddMMyyyy");
//      this.data = sf.parse(strData);
//  }
  
  public void setDataNascimento(Date dataNascimento) {
    this.dataNascimento = dataNascimento;
  }

  public String getLocal() {
    return local;
  }

  public void setLocal(String local) {
    this.local = local;
  }

  public String getCpf() {
    return cpf;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }
}