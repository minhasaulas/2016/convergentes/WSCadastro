package br.edu.up.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DerbyConector implements Conector {
	
	private String driver = "org.apache.derby.jdbc.EmbeddedDriver";
	private String url = "res/UrnaEletronica";
	
	public DerbyConector(String banco) {
		this.driver = "org.apache.derby.jdbc.EmbeddedDriver";
		//jdbc:derby:res/UrnaEletronica
		this.url = "jdbc:derby:res/" + banco;
	}

	public Connection getConexao() throws SQLException, ClassNotFoundException {
		Class.forName(driver);
		return DriverManager.getConnection(url); 
	}
}