package br.edu.up.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLiteConector implements Conector {
	
	private String str;
	private String driver = "org.sqlite.JDBC";
	
	public SQLiteConector(String banco) {
		this.driver = "org.sqlite.JDBC";
		this.str = "res/" + banco;
	}
	
	public SQLiteConector(String caminho, String banco) {
		this.driver = "org.sqlite.JDBC";
		this.str = caminho + banco;
	}

	public Connection getConexao() throws SQLException, ClassNotFoundException {
		Class.forName(driver);
		String url = "jdbc:sqlite:" + str;
		return DriverManager.getConnection(url); 
	}
}