package br.edu.up.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2Conector implements Conector {
	
	private String driver;
	private String url;
	
	public H2Conector(String banco) {
		this.driver = "org.h2.Driver";
		//jdbc:h2:./res/UrnaEletronica;user=sa
		this.url = "jdbc:h2:./res/" + banco + ";user=sa";
	}

	public Connection getConexao() throws SQLException, ClassNotFoundException {
		Class.forName(driver);
		return DriverManager.getConnection(url); 
	}
}