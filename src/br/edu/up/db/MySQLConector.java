package br.edu.up.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConector implements Conector {
	
	private String url;
	private String driver;
	
	public MySQLConector(String banco) {
		this.driver = "com.mysql.jdbc.Driver";
		this.url = "jdbc:mysql://localhost:3306/" + banco 
				 + "?user=root&password=master";
	}

	public Connection getConexao() throws SQLException, ClassNotFoundException {
		Class.forName(driver);
		return DriverManager.getConnection(url); 
	}
}