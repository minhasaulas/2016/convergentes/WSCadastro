package br.edu.up.db;

import java.sql.Connection;
import java.sql.SQLException;

public interface Conector {
	
	Connection getConexao() throws SQLException, ClassNotFoundException;
}
